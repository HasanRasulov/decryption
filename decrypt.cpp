

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <sstream>
#include <utility>
#include <cmath>
#include <map>

#define FILE_N 19

using namespace std::string_literals;

std::pair<size_t,size_t> prime_factors(const std::vector<int>& primes_table,int n){

	for(size_t i=0;i<primes_table.size();i++){

		for (size_t j=i+1;j<primes_table.size();j++){

			if(primes_table[i]*primes_table[j] == n){

				return {primes_table[i],primes_table[j]};
			} 

		}

	}

}

std::pair<size_t,size_t> get_public_key(std::string line){


	size_t comma_pos = line.find(',');
	size_t open_pos = line.find('(');
	size_t close_pos = line.find(')');

	std::string n_str = line.substr(line.find('(') + 1,comma_pos - open_pos);
	std::string e_str = line.substr(comma_pos+2, close_pos - comma_pos);

	return {std::stoul(n_str),std::stoul(e_str)};
}

size_t get_private_key(size_t e,size_t fi){

	e = e % fi;
	for(size_t i = 1; i<fi;i++)
		if((e*i) % fi == 1) 
			return i;

	return 1;


}

size_t powMod(size_t msg,size_t exp,size_t mod){

	size_t res = 1;

	for(size_t i = 0 ;i<exp;i++)
		res = fmod(res*msg,mod); 

	return res;
}


size_t powModNew(size_t msg,size_t exp,size_t mod){

	size_t A = 1;
	size_t m = exp;
	size_t t = msg;

	while( m > 0 )
	{
		size_t k = floor( m/2 );
		size_t r = m - 2*k;
		if( r == 1 )
			A = fmod( A*t, mod );
		t = fmod( t*t, mod );
		m = k;
	}			
	return A;

}

int main(){

	std::ifstream file("primes.txt");
	std::string line;
	std::vector<int> primes(1000);

	if(file.is_open()){

		while (std::getline(file, line))
		{       
			std::istringstream iss(line);
			std::string number;
			while(std::getline(iss,number,'\t')){

				primes.push_back(std::stoi(number));

			}    

		}

		file.close();
	}


	std::map<int,char> alphabet_mapping = {
		{2,'A'},{3,'B'},{4,'C'},{5,'D'},{6,'E'},{7,'F'},{8,'G'},{9,'H'},{10,'I'},{11,'J'},{12,'K'},
		{13,'L'},{14,'M'},{15,'N'},{16,'O'},{17,'P'},{18,'Q'},{19,'R'},{20,'S'},{21,'T'},{22,'U'},
		{23,'V'},{24,'W'},{25,'X'},{26,'Y'},{27,'Z'},{28,'a'},{29,'b'},{30,'c'},{31,'d'},{32,'e'},
		{33,'f'},{34,'g'},{35,'h'},{36,'i'},{37,'j'},{38,'k'},{39,'l'},{40,'m'},{41,'n'},{42,'o'},
		{43,'p'},{44,'q'},{45,'r'},{46,'s'},{47,'t'},{48,'u'},{49,'v'},{50,'w'},{51,'x'},{52,'y'},
		{53,'z'},{54,'~'},{55,'!'},{56,'@'},{57,'#'},{58,'$'},{59,'%'},{60,'^'},{61,'&'},{62,'*'},
		{63,'('},{64,')'},{65,'-'},{66,'+'},{67,'`'},{68,'1'},{69,'2'},{70,'3'},{71,'4'},{72,'5'},
		{73,'6'},{74,'7'},{75,'8'},{76,'9'},{77,'0'},{78,'-'},{79,'='},{80,'<'},{81,'>'},{82,'?'},
		{83,':'},{84,'"'},{85,'{'},{86,'}'},{87,'|'},{88,','},{89,'.'},{90,'/'},{91,';'},{92,'\''},
		{93,'['},{94,']'},{95,'\\'},{96,' '}
	};


	const char* files[] = {"Ex_0000001_RSA.txt","Ex_0000002_RSA.txt","Ex_0000003_RSA.txt",
		"Ex_0000004_RSA.txt","Ex_0000005_RSA.txt","Ex_0000006_RSA.txt",
		"Ex_0000007_RSA.txt","Ex_0000008_RSA.txt","Ex_0000009_RSA.txt",
		"Ex_0000010_RSA.txt","Ex_0000011_RSA.txt","Ex_0000012_RSA.txt",
		"Ex_0000013_RSA.txt","Ex_0000014_RSA.txt","Ex_0000015_RSA.txt",
		"Ex_0000016_RSA.txt","Ex_0000017_RSA.txt","Ex_0000018_RSA.txt",
		"Ex_0000019_RSA.txt" }; 


	for(size_t i = 0;i<FILE_N;i++){ 


		std::ifstream ex("files/"s + files[i]);
		size_t n,e;
               
                std::cout<<"\n\n---------------------------------------------------------------"<<files[i]<<"-------------------------------------------------------\n\n";

		if(ex.is_open()){

			ex.ignore(1000,'=');

			std::string line;
			std::getline(ex,line);

			auto[n,e] = get_public_key(line);
			auto[p,q] = prime_factors(primes,n);
			auto fi = (p - 1)*(q - 1);    
			auto d = get_private_key(e,fi);               

			while(std::getline(ex,line)){

				std::istringstream iss(line);
				std::string number;

				while(std::getline(iss,number,' ')){

					if(line[0]=='/') 
						continue;
					if(!number.empty()){
						auto enc_data = std::stoul(number);
						auto dec_data = powModNew(enc_data,d,n);
						std::cout<<alphabet_mapping[dec_data]; 

					}

				} 


			} 



			ex.close();
		}

	}


	return 0;
}
